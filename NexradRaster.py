#coding=utf-8

import LibConfig
import sys

import numpy
import netCDF4

#import pyproj
from osgeo import gdal
from osgeo import osr


def WriteGrid(dataset, variableName, outRaster, range=230000):

    # Base projection
    center = (dataset.RadarLatitude, dataset.RadarLongitude, dataset.RadarAltitude/3.28083989501312) # Radar altitude is in feets
    projStr = '+proj=aea +lat_1=%f +lat_2=%f +lat_0=%f +lon_0=%f +x_0=0 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs' % (center[0]-1, center[0]+1, center[0], center[1])
    # Proj = pyproj.Proj(projStr)
    # OSR object used for write raster grid
    sr = osr.SpatialReference()
    sr.ImportFromProj4(projStr)

    # It is extremely trick! Netcdf gives incorrect start position?!!!
    # origin_x, origin_y = Proj(dataset.geospatial_lon_max, dataset.geospatial_lat_max)
    if dataset.keywords_vocabulary == "NCZ":
        range *= 2
    origin_x, origin_y = -range, range
    var_x = dataset.variables['x'][:2] * 1000 #in km
    grid_size = var_x[1] - var_x[0]
    half_grid = grid_size / 2.0
    # Because NCZ is using (y, x), we need swap x <-> y
    values = numpy.flipud(dataset.variables[variableName][:])
    driver = gdal.GetDriverByName('HFA')
    dataType = gdal.GDT_Int32 if values.dtype == 'int' else gdal.GDT_Float32
    out = driver.Create(outRaster, dataset.variables['x'].shape[0], dataset.variables['y'].shape[0], 1, dataType)
    # I don't know how this works. Just try and try and try ^_^
    out.SetGeoTransform([origin_x - grid_size, grid_size, 0, - origin_y - grid_size, 0, grid_size])
    band = out.GetRasterBand(1)
    band.WriteArray(values)
    band.SetNoDataValue(-9999)
    out.SetProjection(sr.ExportToWkt())
    # There is no close function. Explicity delete objects
    del out
    del driver
    # TODO: Print out progress
    print "[RASTER] Done"


def ConvertToRaster(inNetcdf, outRaster):
    """We use gdal function to create raster, remember all raster layers are projected.
    :param: inNetcdf, netCDF4.Dataset, Input netCDF dataset
    :param: str, outRaster, path of output raster file.
    """
    dataset = inNetcdf
    if dataset.isRadial == 1:
        raise Exception("Rasterize a radial dataset is not supported")
    if not outRaster.endswith(".img"):
        outRaster += ".img"
    if dataset.keywords_vocabulary in ["NCZ", "NCO", "NC0", "NCR"]:
        WriteGrid(dataset, "BaseReflectivityComp", outRaster)
    else:
        raise NotImplementedError("Write %s as raster is not supported yet." % dataset.keywords_vocabulary)
    return 0


if __name__ == "__main__":
    ConvertToRaster(sys.argv[1], sys.argv[1] + ".img")


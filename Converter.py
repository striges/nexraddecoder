#coding=utf-8
import LibConfig

import sys
import os

from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtCore import pyqtSlot

from NexradDecoder import ConvertToNetCDF, ConvertToFeature, ConvertToRaster

class MyDialog(QtGui.QDialog):    
    
    def __init__(self, toolsUI=None):

        QtGui.QDialog.__init__(self)
        self.ui = uic.loadUi(os.path.join(os.path.dirname(__file__), "Converter.ui"))
        
        # Additional widgets
        self.label = QtGui.QLabel(self.ui)
        self.label.setText("Drag and drop files here to convert")
        self.ui.scrollArea.setWidget(self.label)        
        
        
        # Connect slots
        self.ui.buttonBox.accepted.connect(self.on_buttonBox_accepted)
        self.ui.buttonBox.rejected.connect(self.on_buttonBox_rejected)
        self.ui.setAcceptDrops(True)
        self.ui.dragEnterEvent = self.dragEnterEvent
        self.ui.dropEvent = self.dropEvent
        # Init variables
        self.url = None
        # If toolsUI jar path is not given, use bundled one.
        self.toolsUI = os.path.join(os.path.dirname(__file__), "toolsUI-4.6.jar")
        # Start window
        self.ui.show()

    @pyqtSlot()
    def on_buttonBox_accepted(self):
        #TODO: Do conversion here.        
        for url in self.urls:
            selectedIndex = self.ui.comboBox.currentIndex()
            ret, ncout = ConvertToNetCDF(url, str(self.toolsUI))
            out = ncout
            if selectedIndex == 1:
                ret = ConvertToFeature(ncout, ncout.replace(".nc", ".shp"))
            elif selectedIndex == 2:
                ret = ConvertToRaster(ncout, ncout.replace(".nc", ".img"))
                
            if ret == 0:
                msg = "Converted to %s" % out
            else:
                msg = "Failed %s. Return code is: %d" % (url, ret)
                
            self.label.setText(str(self.label.text()) + "\n" + msg)             
        pass

    @pyqtSlot()
    def on_buttonBox_rejected(self):
        sys.exit(0)
        pass

    def dragEnterEvent(self, event):
        print [p for p in event.mimeData().formats()]
        event.accept()

    def dropEvent(self, event):
        mime = event.mimeData()
        if mime.hasUrls():
            #It has urls, let's convert it.
            self.urls = [str(p.toLocalFile()) for p in mime.urls()]
            print self.urls
            self.label.setText("\n".join(self.urls))

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    win = MyDialog() if len(sys.argv) == 1 else MyDialog(sys.argv[1])
    sys.exit(app.exec_())
#coding=utf-8
import LibConfig

import os
import sys
import argparse
import subprocess
import numpy
import urllib

import netCDF4 as nc
        
from NexradDecoderL3 import ConvertToNetCDF, ConvertToAvro, ConvertToProtobuf

def ConvertToFeature(inNetcdf, outFeature):
    import NexradFeature
    ds = nc.Dataset(inNetcdf)
    # Convert to shapefile
    NexradFeature.ConvertToFeature(ds, outFeature, True)
    # Close netcdf
    ds.close()
    return 0


def ConvertToRaster(inNetcdf, outRaster):
    import NexradRaster
    ds = nc.Dataset(inNetcdf)
    NexradRaster.ConvertToRaster(ds, outRaster)
    return 0


# Main entry here
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("i", help="input nexrad file", type=str)
    parser.add_argument("-t", help="path of toolsUI-4.6.jar", type=str)
    parser.add_argument("-j", "--java", help="use given Java VM", type=str)
    parser.add_argument("-s", "--shapefile", help="create shapefile output", type=str)
    args = parser.parse_args()
    if __debug__:
        print args
    
    # Dump to netcdf    
    inNexrad = args.i
    toolsUI = args.t or os.path.join(os.path.dirname(__file__), 'toolsUI-4.6.jar')
    # if toolsUI-4.5.jar does not exists, let us download one
    if not os.path.exists(toolsUI):
        # Download tools ui to temporary folder. Windows should have %TEMP%, POSIX should have /tmp
        temp_dir = os.environ["TEMP"] if sys.platform == "win32" else "/tmp" 
        toolsUI = os.path.join(temp_dir, "toolsUI-4.6.jar")
        print "Cannot find toolsUI at given path, download one from UCAR FTP, please wait..."
        urllib.urlretrieve("ftp://ftp.unidata.ucar.edu/pub/netcdf-java/v4.6/toolsUI-4.6.jar", toolsUI)
        assert os.path.exists(toolsUI)
    
    ret, outNetcdf = ConvertToNetCDF(inNexrad, toolsUI, args.java)
    # If return is not 0 -> something failed -> skip convert it to avro
    if ret != 0:
        print >> sys.stderr, "error when create netcdf"
        sys.exit(-1) # 1 -> error when create netcdf
    # Convert it to protobuf, it should produce no problem.
    if ConvertToProtobuf(outNetcdf, outNetcdf.replace('.nc', '.protobuf')) != 0:
        print >> sys.stderr, "error when create protobuf"
        sys.exit(-2) # 2 -> error when create protobuf
    # Convert to shapefile if needed
    if args.shapefile and ConvertToFeature(outNetcdf, args.shapefile) != 0:
        print >> sys.stderr, "error when create features"
        sys.exit(-3) # 3 -> error when create features
    # No Error
    sys.exit(0) # 0 -> no error
    print "[MESSAGE]:Done"

if __name__ == "__main__":
    main()


#coding=utf-8

import sys
import os

## This part is tested so it should be fine
SCRIPT_DIR = os.path.dirname(__file__)

# Add Lib folder to search path
LIB_DIR = os.path.join(SCRIPT_DIR, "Lib")

# Append LIB_DIR and all zip files in this folder as search paths
if os.path.exists(LIB_DIR) and (not LIB_DIR in sys.path):
    sys.path.append(LIB_DIR)
    for i in filter(lambda p: p.endswith('zip'), os.listdir(LIB_DIR)):
        if not i in sys.path:
            sys.path.append(i)
else:
    LIB_DIR = None

if __debug__:
    for p in sys.path:
        print "libpath:", p
#coding=utf-8
"""
This file contains function that convert netCDF dataset (pointer) to shapefile
To handle accurate lat/lon issue, we must project them using certain lcc and project it back.
"""

#Add library
import LibConfig

import sys
import math

import numpy

import pyproj
import gdal
import shapefile


def moving_average(a, n=2) :
    ret = numpy.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def RadialFeature(dataset, variableName):
    """Generate radial features from netcdf. If adjust is True, we will adjust bin size based on beam propogation. This would reduce error from 0.5% to 0.05%.
    :param: adjust, boolean, indicate if we want fine tune bin size for each gate
    :param: rotate, boolean, indicate if we want to treat current azimuth as central lines (True) or edges (False)
    """
    # Result container
    results = []

    # Base projection
    center = (dataset.RadarLatitude, dataset.RadarLongitude, dataset.RadarAltitude) # Radar altitude is in METER
    Proj = pyproj.Proj('+proj=lcc +lat_1=%f +lat_2=%f +lat_0=%f +lon_0=%f'
                       ' +x_0=0 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs' % (center[0] - 5, center[0] + 5, center[0], center[1]))
    # Make srs is correct
    assert(abs(Proj(center[1], center[0])[0]) < 0.0001)
    # Calculate gate position on ground
    phi = math.radians(dataset.variables['elevation'][0])
    gate = dataset.variables['gate'][:]
    
    # Calculate bin width
    gate_width = numpy.average(numpy.ediff1d(gate)[1:-1])
    gate = numpy.append(gate, gate[-1] + gate_width)
    # Then we calcualte height for each gate, if gate distance is negative,
    # ignore it.  Remember in Level-III, it is the beginning position of a
    # gate.
    R = gate / 1000.0 # to km
    h = R * math.sin(phi) + R ** 2 / 2 / 1.21 / 6371.0 # in km
    ground_gates = (R ** 2 - h ** 2) ** 0.5 * 1000 # in meter

    # Azimuth
    az = dataset.variables['azimuth'][:]
    # Dump values to memory, so access will be faster then.
    values = dataset.variables[variableName][:]
   
    # Since a complete sweep will contains more than 360deg.  We need cut off
    # at roughly 360
    # So general we need calculate middle of each angle.
    # ----p------ <- beam center
    # --start---- <--beam edge
    # ----q------ <- beam center
    # ---end----- <- beam edge
    # ----r------ <- beam center
    # We know they may overlap at the end.  Just leave it as overlap
    
    # This is the way how we do it.  We first using beam center line as edges,
    # then we rotate `beam-width` to place them in correct position.
    # It is possible the non-uniform beam spacing will cause some error but it
    # should be small.

    d = numpy.abs(numpy.ediff1d(az))
    # Get average beam spacing as beam width.  Filter out jumping value across
    # 0deg
    beam_width = numpy.average(d[d < 5])
    print beam_width

    # p = az[x], q = az[x+1], skip the last one
    p = az[:-1]
    q = numpy.roll(az, -1)[:-1]
    print p.shape[0] == q.shape[0]
    if __debug__:
        print "Crossing 0deg boundary:", p[(p < 360) & (q > 0) & (p > q)], q[(p < 360) & (q > 0) & (p > q)]
    
    # Convert to math radians then rotate
    p = numpy.deg2rad(90 - p + beam_width / 2)
    q = numpy.deg2rad(90 - q + beam_width / 2)

    gate_start = ground_gates[:-1]
    gate_end = ground_gates[1:]

    size = p.shape[0]

    # Draw beams, discard the last beam
    for i in xrange(p.shape[0]):
        start = p[i]
        end = q[i]
        # This loop is SLOW.  Probably we can Cython or llvm it to make it
        # faster
        for j in xrange(gate_start.shape[0]):
            # We simply skip any gate which distance is negative, for unknown
            # reason it is possible
            c = gate_start[j]  
            if c < 0:
                continue
            d = gate_end[j]
            v = values[i,j]
            # Skip on empty value
            if numpy.isnan(v):
                continue
            # Write in this direction
            ## 1 - 2
            ## | | |
            ## | | |
            ## 4 - 3
            ## We need do an inverse projection to get correct lat/lon
            src = numpy.empty((5,2))
            dst = numpy.empty((5,2))
            src[0] = math.cos(start) * d, math.sin(start) * d
            src[1] = math.cos(end) * d, math.sin(end) * d
            src[2] = math.cos(end) * c, math.sin(end) * c
            src[3] = math.cos(start) * c, math.sin(start) * c
            src[4] = src[0]  # Close the polygon
            lons, lats = Proj(src[:,0], src[:,1], inverse=True)  # Gives back lon,lat
            dst[:, 0] = lons
            dst[:, 1] = lats
            # TODO: add heights to make it 3D
            results.append((dst.tolist(), v))

        # Output progress for external usage.
        print "[PROGRESS]%d" % (100 * (i + 1) / size)

    # Simply return results
    return results


def PointMatrixFeature(dataset, variableName, range=230000):
    results = {}

    # Base projection
    center = (dataset.RadarLatitude, dataset.RadarLongitude, dataset.RadarAltitude) # Radar altitude is in feets
    Proj = pyproj.Proj('+proj=aea +lat_1=%f +lat_2=%f +lat_0=%f +lon_0=%f'
                   ' +x_0=0 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs' % (center[0] - 1, center[0] + 1, center[0], center[1]))
    # Make srs is correct
    assert(abs(Proj(center[1], center[0])[0]) < 0.0001)
    # It is extremely trick!  Netcdf gives incorrect start position?!!!
    #origin_x, origin_y = Proj(dataset.geospatial_lon_max,
    #dataset.geospatial_lat_max)
    if dataset.keywords_vocabulary == "NCZ":
        range *= 2
    origin_x, origin_y = -range, range
    var_x = dataset.variables['x'][:] * 1000 #in km
    var_y = dataset.variables['y'][:] * 1000 #in km
    half_grid = (var_x[1] - var_x[0]) / 2.0
    values = dataset.variables[variableName]
    size = var_y.shape[0]
    for i in xrange(size):
        offset_y = var_y[i]
        for j in xrange(var_x.shape[0]):
            offset_x = var_x[j]
            v = values[i, j]
            # Skip invalid number
            if numpy.isnan(v):
                continue
            if v in results:
                results[v].append([origin_x + offset_x - half_grid, origin_y - offset_y + half_grid])
            else:
                results[v] = [[origin_x + offset_x - half_grid, origin_y - offset_y + half_grid]]
        # Print out progress
        print "[PROGRESS]%d" % (100 * (i + 1) / size)

    # Convert coordinates in results back to lon/lat
    for r in results:
        src = numpy.array(results[r], dtype=numpy.float32)
        dst = numpy.empty(src.shape)
        lons, lats = Proj(src[:, 0], src[:, 1], inverse=True)
        dst[:, 0] = lons
        dst[:, 1] = lats
        results[r] = dst.tolist()

    return results


# Base reflectivity
def BaseReflectivityFeature(dataset):
    return RadialFeature(dataset, 'BaseReflectivity')


# Base velocity
def VelocityFeature(dataset):
    return RadialFeature(dataset, 'RadialVelocity')


# 1-hour precipitation
def OneHoursPrecipitationFeature(dataset):
    return RadialFeature(dataset, 'Precip1hr')


# 3-hour precipitation
def ThreeHoursPrecipitationFeature(dataset):
    # TODO: Check product name
    return RadialFeature(dataset, 'Precip3hr')


# Composite reflectivity
def CompositeReflectivityFeature(dataset):
    return PointMatrixFeature(dataset, "BaseReflectivityComp")


def ConvertToRadialFeature(dataset, out_shp):
    # Get product code
    product_type = dataset.keywords_vocabulary
    # This is base reflectivity
    if product_type in ['N0R', 'NOR', 'N1R', 'N2R', 'N3R']:
        geometry_collection = BaseReflectivityFeature(dataset)
    elif product_type in ['N0V', 'NOV', 'N1V', 'N2V', 'N3V']:
        geometry_collection = VelocityFeature(dataset)
    elif product_type == 'N1P':
        geometry_collection = OneHoursPrecipitationFeature(dataset)
    elif product_type == 'N3P':
        geometry_collection = ThreeHoursPrecipitationFeature(dataset)
    else:
        raise NotImplementedError("Unsupported radial product: %s" % product_type)

    # Create shapefile center
    shp = shapefile.Writer(shapefile.POLYGON)
    if product_type in ['N0R', 'N1R', 'N2R', 'N3R']:
        shp.field("VALUE", "N", 8)
    elif product_type in ['N0V', 'N1V', 'N2V', 'N3V']:
        shp.field("VALUE", "N", 8)
    else:
        shp.field("VALUE", "N", 8)
    for g in geometry_collection:
        shp.poly([g[0]])
        shp.record(g[1])
    shp.save(out_shp)

    # Finally we need save a WGS84 projection file
    prj = open(out_shp[:-3] + "prj", 'w')
    #prj.write('GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]]')
    prj.write('GEOGCS["GCS_North_American_1983",DATUM["D_North_American_1983",SPHEROID["GRS_1980",6378137,298.257222101]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]]')
    prj.close()
    pass


def ConvertToPointFeature(dataset, out_shp):
    """That is the same problem with raster. Since the dataset gives origin as (lon,lat) but resolution is (km, km),
in point matrix, we may able to project it back to GCS without problem but in raster, we have to keep it projected.
    """
    # Get product code
    productType = dataset.keywords_vocabulary
    if productType in ['NCO', 'NC0', 'NCR', 'NCZ']:
        geometryCollection = CompositeReflectivityFeature(dataset)
    else:
        raise Exception("Unsupported grid product: %s" % productType)

    # NOTE: we use multipoint to save space, so type of geometryCollection is a
    # dict of {value: [points]}
    shp = shapefile.Writer(shapefile.MULTIPOINT)
    shp.field("VALUE", "N", 8)

    for g in geometryCollection:
        shp.poly([geometryCollection[g]], shapeType=shapefile.MULTIPOINT)
        shp.record(g)
    shp.save(out_shp)

    #Finally we need save a WGS84 projection file
    prj = open(out_shp[:-3] + "prj", 'w')
    #prj.write('GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]]')
    prj.write('GEOGCS["GCS_North_American_1983",DATUM["D_North_American_1983",SPHEROID["GRS_1980",6378137,298.257222101]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]]')
    prj.close()


def ConvertToFeature(dataset, out_shp):
    try:
        # If the archive is Level-II, we need perform Level-II actions

        # Else, if it is level-III, we need perform Level-III actions
        # Strange, isRadial == 1 is radial.  Others still have non-zero values
        print dataset.__dict__
        if (hasattr(dataset, 'isRadial') and int(dataset.isRadial) == 1) or \
            (hasattr(dataset, 'cdm_data_type') and dataset.cdm_data_type == "RADIAL") or \
            (hasattr(dataset, 'DataType') and dataset.DataType == "Radial"):
            ConvertToRadialFeature(dataset, out_shp)
        else:
            ConvertToPointFeature(dataset, out_shp)
        return 0
    except Exception, ex:
        if __debug__:
            raise ex
        else:
            sys.stderr.write(ex.message)
            return -3


# Test:
if __name__ == "__main__":
    import netCDF4
    ds = netCDF4.Dataset(sys.argv[1])
    ConvertToFeature(ds, sys.argv[1] + ".shp")